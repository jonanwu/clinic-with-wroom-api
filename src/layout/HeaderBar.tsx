import { useFirstPathname } from "@/hooks/otherHooks"
import usePageTitle from "@/hooks/usePageTitle"
import { AppBar, Box, Stepper, Toolbar, Typography } from "@mui/material"
import React, { PropsWithChildren, useMemo } from "react"

const BookingStepper = React.lazy(() => import("@/components/BookingStepper"))

type Props = PropsWithChildren

function AppBarWrao(props: Props) {
  const title = usePageTitle()

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Box sx={{ width: "100%", p: 1 }}>
          <Typography
            sx={{ width: "100%", textAlign: "center" }}
            variant="h6"
            color="inherit"
            component="div"
          >
            {title}
          </Typography>
          {props.children && <Box sx={{ width: "100%", mb: 1, mt: 1 }}>{props.children}</Box>}
        </Box>
      </Toolbar>
    </AppBar>
  )
}

export default function HeaderBar() {
  const pathname = useFirstPathname()
  const children = useMemo(() => {
    switch (pathname) {
      case "booking":
        return (
          <Stepper activeStep={0}>
            <BookingStepper />
          </Stepper>
        )
      case "register":
      case "login":
      case "":
      default:
        return null
    }
  }, [pathname])

  return <AppBarWrao>{children}</AppBarWrao>
}
