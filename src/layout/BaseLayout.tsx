import { Box, Container, CssBaseline, ThemeProvider, createTheme } from "@mui/material"
import { PropsWithChildren, useEffect } from "react"
import { LocalizationProvider } from "@mui/x-date-pickers"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import LoadingMask from "@/components/LoadingMask"
import { ComponentsStateProvider } from "@/hooks/useComponentsContext"
import FeedbackBar from "@/components/FeedbackBar"
import { Provider as StoreProvider } from "react-redux"
import { store } from "@/store/store"
import "dayjs/locale/zh-tw"
import HeaderBar from "./HeaderBar"
import Helmet from "@/components/Helmet"
import NavigatorFloatButton from "@/components/NavigatorFloatButton"
import { useRouter } from "next/router"

const theme = createTheme({
  palette: {
    primary: {
      main: "#0052cc",
    },
    secondary: {
      main: "#edf2ff",
    },
  },
})

export default function BaseLayout(props: PropsWithChildren) {
  const router = useRouter()
  useEffect(() => {
    console.log("router.isReady", router.isReady)
  }, [router])
  return (
    <StoreProvider store={store}>
      <ThemeProvider theme={theme}>
        <ComponentsStateProvider>
          <LocalizationProvider dateAdapter={AdapterDayjs} locale="zh-tw">
            <HeaderBar />
            <Container component="main" maxWidth="xs">
              <Helmet />
              <CssBaseline />
              <Box
                sx={{
                  marginTop: 0,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  mb: 5,
                }}
              >
                {props.children}
              </Box>
              <LoadingMask />
              <FeedbackBar />
              <NavigatorFloatButton />
            </Container>
          </LocalizationProvider>
        </ComponentsStateProvider>
      </ThemeProvider>
    </StoreProvider>
  )
}
