import { useAppSelector } from "@/hooks/otherHooks"
import { CircularProgress } from "@mui/material"
import { signIn, useSession } from "next-auth/react"
import { Fragment, PropsWithChildren, useEffect, useMemo } from "react"

export default function AuthLayout({ children }: PropsWithChildren) {
  const session = useSession()

  const isAuthenticated = useMemo(() => session.status === "authenticated", [session.status])
  const testRedux = useAppSelector((s) => s.components.test)
  useEffect(() => {
    console.log(testRedux.text)
  }, [testRedux.text])

  useEffect(() => {
    if (session.status === "loading") return
    if (!isAuthenticated) signIn()
  }, [isAuthenticated, session.status])

  if (isAuthenticated) {
    return <Fragment>{children}</Fragment>
  }

  return <CircularProgress />
}
