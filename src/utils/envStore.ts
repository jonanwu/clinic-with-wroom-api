type EnvKeyType =
  | "DATABASE_URL"
  | "JWT_SECRET"
  | "MAIL_APP_USERNAME"
  | "MAIL_APP_PASSWORD"
  | "MAIL_SERVICE_NAME"

const EnvStore = {
  get(key: EnvKeyType) {
    return process.env[key]
  },
}

export default EnvStore
