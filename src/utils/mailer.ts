import nodemailer from "nodemailer"
import EnvStore from "./envStore"

export const transporter = nodemailer.createTransport({
  service: EnvStore.get("MAIL_SERVICE_NAME"),
  auth: {
    user: EnvStore.get("MAIL_APP_USERNAME"),
    pass: EnvStore.get("MAIL_APP_PASSWORD"),
  },
})
