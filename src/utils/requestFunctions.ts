import { WroomRegular } from "@/constants/Others"
import $request from "./request"
import dayjs from "dayjs"

export async function getScheduleByDate(date: string) {
  const params: WroomCommonTypes.GetScheduleRequestParam = {
    ClinicCode: WroomRegular.ClinicCode,
    StartDate: date,
    EndDate: date,
  }
  const resp = await $request<"WroomApiDocs", WroomCommonTypes.GetScheduleReponseData, unknown>(
    "WroomApiDocs",
    "GetSchedule",
    {
      method: "GET",
      params,
    }
  )

  const schedule = resp.data.Data.map((s) => s.Sches)
    .flat()
    .sort((a, b) => a.Tsec - b.Tsec)
    .map((s, i) => ({ ...s, Index: i }))

  return schedule
}

export async function getReservation(
  _params: Omit<WroomCommonTypes.ParamsReservationGet, "ClinicCode" | "StartDate" | "EndDate">
) {
  const params: WroomCommonTypes.ParamsReservationGet = {
    ClinicCode: WroomRegular.ClinicCode,
    CId: _params.CId || undefined,
    StartDate: dayjs()
      .subtract(3, "month")
      .startOf("M")
      .startOf("day")
      .format(WroomRegular.DateFormat),
    EndDate: dayjs()
      //
      .add(3, "month")
      .endOf("M")
      .endOf("day")
      .format(WroomRegular.DateFormat),
  }

  const resp = await $request<
    "WroomApiDocs",
    WroomCommonTypes.ResponseDataReservationGet[],
    WroomCommonTypes.ParamsReservationGet
  >("WroomApiDocs", "Reservation", {
    method: "GET",
    params,
  })

  return resp.data
}
