export type ErrorScope = "REGISTER" | "LOGIN" | "BOOKING" | "QUERY"
export type ErrorType = "Prisma" | "WroomcApi" | "LocalApi" | "Others"

export type CommonErrorConstractorProps = {
  message: string
  code: string
  scope: ErrorScope
  type?: ErrorType
  error: unknown
}

export default class CommonError extends Error {
  Code: string
  Scope: ErrorScope
  Message: string
  Type: ErrorType

  constructor(props: CommonErrorConstractorProps) {
    super(props.message)
    this.Scope = props.scope
    this.Code = props.code
    this.Message = props.message
    if (props.message.includes("prisma")) this.Type = "Prisma"
    else this.Type = "Others"
    Object.setPrototypeOf(this, CommonError.prototype)
  }

  getErrorJson() {
    return {
      Message: this.Message,
      Code: this.Code,
      Scope: this.Scope,
      Type: this.Type,
    }
  }
}
