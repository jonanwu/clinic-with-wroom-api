import ApiMap from "@/constants/ApiDocs"
import axios, { AxiosError, AxiosRequestConfig } from "axios"
import DummyStore from "./dummyStore"
import CertificateSecret from "@/constants/CertificateSecret"

const request = axios.create({})

const WroomApi = axios.create({
  baseURL: "https://cloudregapi.vision.com.tw",
  headers: {
    "X-DEVKEY": CertificateSecret.xDevKey,
    "Content-Type": "application/json",
  },
})

WroomApi.interceptors.request.use(
  (config) => {
    console.log(`請求 ${config.url}`)

    const isToAuthenticate = config.url === ApiMap.WroomApiDocs.Authenticate
    const token = DummyStore.get("WroomApiToken")
    if (isToAuthenticate) {
      config.headers.set("Authorization", CertificateSecret.Authorization)
    } else {
      config.headers.set("Mode", CertificateSecret.Mode)
      if (token) config.headers.set("Token", token as string)
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

WroomApi.interceptors.response.use(
  (response) => response,
  async (error: AxiosError<{ Message: string }>) => {
    const errorMessage = error.response?.data?.Message
    const status = error.response?.status
    const statusText = error.response?.statusText

    const isToReAuthenticateConditions = [
      //
      errorMessage === "An error has occurred.",
      status === 401,
      statusText === "Unauthorized",
    ].some((c) => c)

    if (isToReAuthenticateConditions) {
      const resp = await WroomApi.get(ApiMap.WroomApiDocs.Authenticate)
      const token = resp.headers?.token
      DummyStore.set("WroomApiToken", token)
      // throw Promise.reject(new AxiosError(Others.ErrorCode.RETRY, Others.ErrorCode.RETRY))
      const requestConfig = error.config as AxiosRequestConfig
      return WroomApi.request(requestConfig)
    } else {
      throw error
    }
  }
)

export default request
export { WroomApi }
