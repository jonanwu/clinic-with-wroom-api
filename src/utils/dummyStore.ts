/* eslint-disable @typescript-eslint/no-unused-vars */
export const dummyStoreKeys = {
  WroomApiToken: "WroomApiToken",
}
type DummyStoreKeyType = keyof typeof dummyStoreKeys
type DummyStoreType = Partial<Record<DummyStoreKeyType, unknown>>

const DummyStore = {
  _store: {} as DummyStoreType,
  get(key: DummyStoreKeyType) {
    return this._store[key]
  },
  set(key: DummyStoreKeyType, value: unknown) {
    this._store[key] = value
  },
  remove(key: DummyStoreKeyType) {
    // eslint-disable-next-line no-unused-vars
    const { [key]: _, ...rest } = this._store
    this._store = { ...rest }
  },
  setToken(token: string, id: number) {
    const key = `${dummyStoreKeys.WroomApiToken}-${id}` as DummyStoreKeyType
    this.set(key, token)
  },
  getToken(id: string) {
    const key = `${dummyStoreKeys.WroomApiToken}-${id}` as DummyStoreKeyType
    return this.get(key)
  },
  removeToken(id: string) {
    const key = `${dummyStoreKeys.WroomApiToken}-${id}` as DummyStoreKeyType
    this.remove(key)
  },
}

export default DummyStore
