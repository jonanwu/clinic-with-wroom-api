import ApiMap from "@/constants/ApiDocs"
import LocalApi, { WroomApi } from "./services"
import { AxiosRequestConfig, Method } from "axios"
type ServiceNameType = keyof typeof ApiMap
type ApiKeyType<S extends ServiceNameType> = keyof (typeof ApiMap)[S]

type $RequestParamType<S extends ServiceNameType, R> = [
  S | [S, ...string[]],
  ApiKeyType<S>,
  Omit<AxiosRequestConfig<R>, "method"> & {
    method: Method
  }
]

export default async function $request<S extends ServiceNameType, D = unknown, R = unknown>(
  ...params: Required<$RequestParamType<S, R>>
) {
  const [serviceName, apiKey, config] = params
  // eslint-disable-next-line no-useless-catch
  try {
    const url = (() => {
      switch (typeof serviceName) {
        case "string": {
          return ApiMap[serviceName][apiKey] as string
        }
        case "object": {
          const [base, ...params] = serviceName
          return [ApiMap[base][apiKey] as string, ...params].join("/")
        }
      }
    })()

    const instance = (() => {
      switch (serviceName) {
        case "WroomApiDocs": {
          return WroomApi
        }
        case "LocalApiDocs": {
          return LocalApi
        }
      }
      return LocalApi
    })()

    const resp = await instance.request<D>({
      ...config,
      url,
    })

    return resp
  } catch (error) {
    throw error
  }
}
