import { User } from "@prisma/client"
import { Session } from "next-auth"
import { AppProps } from "next/app"
import { useState } from "react"

export type RegisterRequestBody = Omit<User, "id">

export type CustomAppProps<T = unknown> = AppProps<{ session: Session; data?: T }>

type BookingParamsType = {
  date?: string
  scheIndex?: number
}

export type ParamUseStateType = ReturnType<typeof useState<BookingParamsType>>

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace LocalApi {
  export type BookingPayload = WroomCommonTypes.BookRequestPayload
}

export type BookingPayloadSet = ReturnType<typeof useState<Partial<LocalApi.BookingPayload>>>
