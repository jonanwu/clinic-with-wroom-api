/* eslint-disable no-unused-vars */
declare namespace WroomCommonTypes {
  export type Error = {
    Message: string
  }
  export type GetScheduleRequestParam = {
    ClinicCode: string
    StartDate?: string //2017-04-17
    EndDate?: string //2017-04-18
    SubjectCode?: string
  }

  /**
   * @description 0:上午診 1:下午診 2:晚上診 3:夜診診 4:不分時段
   */
  export type TsecType = 0 | 1 | 2 | 3 | 4

  /**
   * @property DoctorId 醫師ＩＤ
   * @property DoctorName 醫生姓名
   * @property Tsec 看診時段
   * @property Count 已掛號人數
   * @property DisplayColor 醫生色碼
   * @property SubjectCode 醫師科別代號
   * @property SubjectName 醫師科別名稱
   * @property DoctorCode 醫師代碼
   * @property MedRoomId 診間ＩＤ
   * @property MedRoomName 診間名稱
   */
  export type Sche = {
    Index?: number
    DoctorId: string
    DoctorName: string
    Tsec: TsecType
    Count: number
    DisplayColor: string
    SubjectCode: string
    SubjectName: string
    DoctorCode: string
    MedRoomId: string
    MedRoomName: string
  }

  /**
   * @property Date 看診日期
   * @property Sches 班表詳細資料
   * @property CanReservation 是否可掛號
   * @property TsecList 今日門診時段
   */
  export type Schedule = {
    Date: string
    Sches: Sche[]
    CanReservation: boolean
    TsecList: TsecType[]
  }

  export type GetScheduleReponseData = {
    Data: Schedule[]
  }

  /**
   * @property ReservationDate 掛號日期
   * @property Tsec 掛號時段
   * @property DoctorId 掛號醫師 ID 識別碼
   * @property CName 病患姓名
   * @property Phone 病患電話號碼
   * @property CId 病患身分證號
   * @property Birthday 病患生日
   */
  export type BookRequestPayload = {
    ReservationDate?: string
    Tsec: TsecType
    DoctorId: string
    CName: string
    Phone: string
    CId: string
    Birthday: string
  }

  /**
   * @description 掛號回應 預約號碼
   */
  export type ResponseMessageData = {
    Message: string
  }

  export type ParamsReservationGet = {
    CId?: string
    ClinicCode: string
    StartDate: string
    EndDate: string
  }

  export type ResponseDataReservationGet = {
    Id: string
    ClinicCode: string
    Tsec: number
    MedRoomCode: string
    DoctorCode: string
    Number: number
    CId: string
    CName: string
    Phone: string
    Birthday: string
    ReservationDate: string
    IsCancel: boolean
  }
}
