import { User as PrismaUser } from "@prisma/client"
import { DefaultSession } from "next-auth"

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  export interface Session {
    user: Omit<PrismaUser, "password" | "first_name" | "last_name"> & {
      name: string
    }
    expires: DefaultSession["expires"]
  }
}
