const WroomApiDocs = {
  Authenticate: "/api/Authenticate",
  GetSchedule: "/api/DoctorsMonthSchedule/GetSchedule",
  LogUpdateHistory: "/api/SoftwareUpdate/LogUpdateHistory",
  Patient: "/api/Patient",
  Reservation: "/api/Reservation",
}

const LocalApiDocs = {
  register: "/api/register",
  book: "/api/booking",
}

const ApiMap = {
  WroomApiDocs,
  LocalApiDocs,
}

export default ApiMap
