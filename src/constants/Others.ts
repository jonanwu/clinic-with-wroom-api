export const WroomRegular = {
  ClinicCode: "06270",
  TsecMap: {
    0: "上午診",
    1: "下午診",
    2: "晚上診",
    3: "夜診診",
    4: "不分時段",
  },
  DateFormat: "YYYY-MM-DD",
}

const Others = {
  WroomRegular,
  clinicCode: "06270",
  ErrorCode: {
    RETRY: "RETRY",
  },
  responseMessage: {
    success: "成功",
    failed: "失敗",
    register: {
      success: "註冊成功",
      failed: "註冊失敗",
      missing_field: "缺少欄位或資料不正確",
      passwordValidateFailed: "密碼不一致",
    },
    login: {
      success: "登入成功",
      failed: "登入失敗",
    },
  },
  reponseCode: {
    success: "success",
    failed: "failed",
  },
  headTitle: {
    register: "基本資料登記",
    login: "身份登入",
    booking: "掛號",
    "": "預約掛號系統",
  },
  bookingStep: {
    pickdate: "選擇日期",
    pickdoctor: "選擇門診",
    success: "掛號確認",
  },
}

export default Others
