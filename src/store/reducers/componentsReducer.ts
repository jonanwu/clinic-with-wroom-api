import { Action, Reducer } from "@reduxjs/toolkit"
import { ComponentAction, ComponentActionType, ComponentsStateType } from "../types"

export const initialState: ComponentsStateType = {
  feedbackProps: {
    key: "GlobalSnackbar",
    anchorOrigin: {
      vertical: "top",
      horizontal: "right",
    },
    alertSeverity: "success",
  },
  others: {
    bookingActiveStep: 0,
  },
  test: {
    text: "",
  },
}

const componentsReduer: Reducer<typeof initialState, Action<ComponentActionType>> = (
  state = initialState,
  _action
): ComponentsStateType => {
  const action = _action as ComponentAction
  switch (action.type) {
    case "UPDATE": {
      return {
        ...state,
        [action.target]: {
          ...initialState[action.target],
          ...action.payload,
        },
      }
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default componentsReduer
