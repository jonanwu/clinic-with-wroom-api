import { AlertProps, SnackbarProps } from "@mui/material"

export type ComponentsStateType = {
  feedbackProps: SnackbarProps & {
    alertSeverity?: AlertProps["severity"]
  }
  others: {
    bookingActiveStep: number
  }
  test: {
    text: string
  }
}

export type ComponentActionType = "UPDATE" | "OPEN" | "CLOSE"
export type ComponentStateKey = keyof ComponentsStateType

export type ComponentAction = {
  type: ComponentActionType
  target: ComponentStateKey
  payload: ComponentsStateType[ComponentStateKey]
}
