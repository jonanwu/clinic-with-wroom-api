import { ComponentAction } from "./types"

export function updateProps(
  type: ComponentAction["type"],
  target: ComponentAction["target"],
  payload: ComponentAction["payload"]
) {
  return { type, target, payload }
}
