import { Box } from "@mui/material"
import { PrismaClient, User as UserType } from "@prisma/client"
import { AppContext } from "next/app"

type PropsType = {
  data: {
    user: UserType
  }
}

export default function User(props: PropsType) {
  return <Box>{JSON.stringify(props.data.user)}</Box>
}

const prisma = new PrismaClient()
export async function getServerSideProps(context: AppContext["ctx"]) {
  const user = await prisma.user.findFirst({
    where: {
      id: Number(context.query.id),
    },
  })

  return {
    props: {
      data: {
        user,
      },
    },
  }
}
