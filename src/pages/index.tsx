/* eslint-disable @typescript-eslint/no-unused-vars */
import Others from "@/constants/Others"
import $request from "@/utils/request"
import { GetServerSidePropsContext } from "next"
import { Box, Divider } from "@mui/material"
import ProfileCard from "@/components/ProfileCard"
import { useRouter } from "next/router"
import { useButtonClickCallback } from "@/hooks/otherHooks"
import CommonButton from "@/components/common/CommonButton"
import { getServerSession } from "next-auth"
import { authOptions } from "./api/auth/[...nextauth]"
import { getReservation } from "@/utils/requestFunctions"

type PropsType = {
  data: { doctorsMonthSchedule: WroomCommonTypes.GetScheduleReponseData }
}
export default function HomePage(props: PropsType) {
  const router = useRouter()
  const onBookingButtonClick = useButtonClickCallback(
    (ev) => {
      ev.preventDefault()
      router.push("/booking/0")
    },
    [router]
  )

  return (
    <Box sx={{ minWidth: 300, marginTop: 5 }}>
      <ProfileCard />
      <Divider sx={{ m: 5 }} />
      <CommonButton onClick={onBookingButtonClick}>我要預約看診</CommonButton>
      <CommonButton>查看歷史</CommonButton>
    </Box>
  )
}

HomePage.auth = true

export async function getServerSideProps(context: globalThis.Next.GetServerSidePropsContext) {
  const session = await getServerSession(context.req, context.res, authOptions)
  console.log("session", session)

  const respData = await getReservation({})

  const reservations = respData.filter((r) => r.CId === session?.user.identity_id)
  return {
    props: {
      data: {
        reservations,
      },
    },
  }
}
