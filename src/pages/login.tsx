import Button from "@mui/material/Button"
import TextField from "@mui/material/TextField"
import Link from "@mui/material/Link"
import Grid from "@mui/material/Grid"
import Box from "@mui/material/Box"
import { getCsrfToken, signIn } from "next-auth/react"
import { AppContext } from "next/app"
import { useRouter } from "next/router"
import useFormSubmitCallback from "@/hooks/useFormSubmitCallback"

export default function LoginPage() {
  const router = useRouter()
  const handleSubmit = useFormSubmitCallback(async (event) => {
    const payload = new FormData(event.currentTarget)
    const credentials = {
      username: payload.get("username"),
      password: payload.get("password"),
    }
    const resp = await signIn("credentials", {
      redirect: false,
      ...credentials,
    })
    if (resp?.ok) {
      return {
        isSuccess: true,
        message: "成功登入",
        callback() {
          router.push("/")
        },
      }
    } else {
      return {
        isSuccess: false,
        message: "「帳號不存在」或「密碼錯誤」",
      }
    }
  })

  return (
    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
      <TextField
        margin="normal"
        required
        fullWidth
        label="身分證字號"
        name="username"
        type="text"
        autoFocus
        defaultValue="A123456789"
      />
      <TextField
        margin="normal"
        required
        fullWidth
        name="password"
        label="密碼"
        type="password"
        autoComplete="current-password"
        defaultValue="123456"
      />
      <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
        登入
      </Button>
      <Grid container>
        <Grid item xs>
          <Link href="#" variant="body2">
            忘記密碼?
          </Link>
        </Grid>
        <Grid item>
          <Link href="/register" variant="body2">
            {"沒帳號嗎？註冊"}
          </Link>
        </Grid>
      </Grid>
    </Box>
  )
}

export async function getServerSideProps(context: AppContext["ctx"]) {
  return {
    props: {
      csrfToken: await getCsrfToken(context),
    },
  }
}
