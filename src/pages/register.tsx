import { Box, Button, Grid } from "@mui/material"
import { useCallback, useState } from "react"
import useFormSubmitCallback from "@/hooks/useFormSubmitCallback"
import request from "@/utils/services"
import ApiMap from "@/constants/ApiDocs"
import { RegisterRequestBody } from "@/types/common"
import FormTextField, { FormTextFieldProps } from "@/components/FormTextField"
import { User } from "@prisma/client"
import Others from "@/constants/Others"
import { AxiosError } from "axios"
import { useRouter } from "next/router"

const formContentItems: (FormTextFieldProps & { sm?: number })[] = [
  {
    label: "姓氏",
    name: "last_name",
    required: true,
    type: "text",
    sm: 6,
  },
  {
    label: "名字",
    name: "first_name",
    required: true,
    type: "text",
    sm: 6,
  },
  {
    label: "身分證字號",
    name: "identity_id",
    regex: /\w\d{9,9}/g,
    helperText: "登入帳號",
    required: true,
  },
  {
    label: "生日",
    name: "birth_date",
    customType: "datepicker",
    type: "date",
    helperText: "點擊選擇日期",
    required: true,
  },
  {
    label: "手機",
    name: "phone",
    regex: /(\+886|0)9\d{8,8}/g,
    type: "tel",
    helperText: "Ex: (+886/0)987654321 台灣門號",
    required: true,
  },
  {
    label: "信箱",
    name: "email",
    regex: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/g,
    type: "email",
    helperText: "Ex: example@gmail.com",
    required: true,
  },
  {
    label: "密碼",
    name: "password",
    regex: /(\w|\W|\d|[@*!#]){6,20}/g,
    autoComplete: "current-password",
    helperText: "僅接受英文大小寫數字符號(@*!#)，長度介於6~20",
    type: "password",
    required: true,
    sm: 6,
  },
  {
    label: "密碼驗證",
    name: "password-validate",
    regex: /(\w|\W|\d|[@*!#]){6,20}/g,
    helperText: "請再輸入一次密碼",
    type: "password",
    required: true,
    sm: 6,
  },
]

const initialValidMarks = formContentItems
  .filter((item) => item.required)
  .reduce(
    (pre, cur) => ({
      ...pre,
      [cur.name as never]: true,
    }),
    {}
  ) as Partial<Record<keyof User, boolean>>

export default function RegisterPage() {
  const [valids, validsSet] = useState(initialValidMarks)
  const generateValidSet = useCallback(
    (key: keyof typeof valids) => (isValid: boolean) => {
      validsSet((s) => ({ ...s, [key]: isValid }))
    },
    []
  )

  const router = useRouter()

  const handleSubmit = useFormSubmitCallback(async (event) => {
    try {
      const from = new FormData(event.currentTarget)
      const invalidMarks = Object.keys(valids).filter((k) => !valids[k as keyof typeof valids])
      if (invalidMarks.length) {
        const [firstMark] = invalidMarks
        const firstErrorMark = formContentItems.find((item) => item.name === firstMark)
        return {
          isSuccess: false,
          message: `${Others.responseMessage.register.missing_field}: ${firstErrorMark?.label}`,
        }
      }

      const password = from.get("password")?.toString()
      const passwordValidate = from.get("password-validate")?.toString()

      if (password !== passwordValidate) {
        return {
          isSuccess: false,
          message: Others.responseMessage.register.passwordValidateFailed,
        }
      }
      const payload: RegisterRequestBody = formContentItems
        .map((i) => i.name)
        .reduce((pre, cur) => {
          const value = (() => {
            switch (cur as keyof User) {
              case "birth_date": {
                return from
                  .get(cur as string)
                  ?.toString()
                  .replaceAll(/\s[年月]\s/g, "-")
                  .replaceAll(/\s[日]/g, "")
              }
              case "password-validate" as never: {
                return undefined
              }
              default: {
                return from.get(cur as string)?.toString()
              }
            }
          })()
          return {
            ...pre,
            [cur as keyof User]: value,
          }
        }, {}) as never

      await request.put(ApiMap.LocalApiDocs.register, payload)

      return {
        isSuccess: true,
        message: "成功註冊",
        callback() {
          router.push("/")
        },
      }
    } catch (error) {
      const __error = error as AxiosError<{ Message: string }>
      return {
        isSuccess: false,
        message: __error.response?.data.Message,
      }
    }
  })

  return (
    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
      <Grid container spacing={2}>
        {formContentItems.map((item, index) => {
          const { sm } = item
          return (
            <Grid key={`register-${index}-${item.name}`} item xs={12} sm={sm}>
              <FormTextField
                validState={
                  (item.required
                    ? { validSet: generateValidSet(item.name as never) }
                    : undefined) as never
                }
                {...item}
              />
            </Grid>
          )
        })}
      </Grid>
      <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
        送出
      </Button>
    </Box>
  )
}
