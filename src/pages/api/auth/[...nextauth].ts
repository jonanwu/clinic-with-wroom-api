import NextAuth, { NextAuthOptions } from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options
export const authOptions: NextAuthOptions = {
  // https://next-auth.js.org/configuration/providers/oauth
  providers: [
    CredentialsProvider({
      name: "身份登入",
      credentials: {
        username: { label: "身分證字號", type: "text", placeholder: "A123456789" },
        password: { label: "密碼", type: "password" },
      },
      async authorize(credentials) {
        const user = await prisma.user.findFirst({
          where: {
            identity_id: credentials?.username,
            password: credentials?.password,
          },
          select: {
            first_name: true,
            last_name: true,
            id: true,
            identity_id: true,
            birth_date: true,
            phone: true,
            email: true,
          },
        })

        if (user) {
          const output = {
            id: user.id,
            name: `${user.last_name}${user.first_name}`,
            identity_id: user.identity_id,
            birth_date: user.birth_date,
            email: user.email,
            phone: user.phone,
          }

          return output as never
        } else {
          return null
        }
      },
    }),
  ],
  secret: process.env.JWT_SECRET,
  pages: {
    signIn: "/login",
  },
  callbacks: {
    async signIn({ user }) {
      console.log(`用戶ID: ${user.id} 登入`)
      try {
        return true
      } catch (error) {
        return false
      }
    },
    async jwt(props) {
      if (props.user) {
        // console.log("jwt(props)", props)
        return {
          ...props.token,
          ...props.user,
        }
      }
      return props.token
    },
    async session(props) {
      // console.log("session(props)", props)
      return {
        ...props.session,
        user: props.token as never,
      }
    },
  },
}

export default NextAuth(authOptions)
