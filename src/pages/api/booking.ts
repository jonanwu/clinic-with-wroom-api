import $request from "@/utils/request"
import { AxiosError } from "axios"
import { NextApiRequest, NextApiResponse } from "next"

type RequestType = Omit<NextApiRequest, "body"> & {
  body: WroomCommonTypes.BookRequestPayload
}

const handler = async (req: RequestType, res: NextApiResponse) => {
  try {
    const payload = { ...req.body } as WroomCommonTypes.BookRequestPayload

    const resp = await $request<"WroomApiDocs", unknown, WroomCommonTypes.BookRequestPayload>(
      "WroomApiDocs",
      "Reservation",
      {
        method: "POST",
        params: {
          cloud: "",
        },
        data: {
          ...payload,
        },
      }
    )

    console.log(resp)

    res.status(200).json(resp.data)
  } catch (error) {
    const _error = error as AxiosError
    const requestPayload = JSON.parse(_error.config?.data)
    // eslint-disable-next-line @typescript-eslint/ban-types
    const responseData = _error.response?.data as unknown as {}
    res.status(_error.response?.status || 500).json({ ...responseData, requestPayload })
  }
}

export default handler
