import { NextApiHandler } from "next"
import { PrismaClient } from "@prisma/client"
import { RegisterRequestBody } from "@/types/common"
import Others from "@/constants/Others"
import CommonError, { CommonErrorConstractorProps } from "@/utils/error"

const prisma = new PrismaClient()

const handler: NextApiHandler = async (_req, res) => {
  try {
    const body = _req.body as RegisterRequestBody
    await prisma.user.create({ data: body })
    await res.status(200).json({
      data: body,
      code: Others.reponseCode.success,
    })
  } catch (__error) {
    const error = __error as CommonErrorConstractorProps
    const commonError = new CommonError({
      message: error.message,
      code: error.code,
      scope: "REGISTER",
      error: __error,
    })
    console.log(commonError instanceof CommonError)

    await res.status(400).json(commonError.getErrorJson())
  }
}

export default handler
