import BookingConfirmation from "@/components/BookingConfirmation"
import BookingPickDate from "@/components/BookingPickDate"
import BookingPickSchedule from "@/components/BookingPickSchedule"
import CommonButton from "@/components/common/CommonButton"
import PageBox from "@/components/common/PageBox"
import { useButtonClickCallback, useFeedbackBar } from "@/hooks/otherHooks"
import useBookingStepper from "@/hooks/useBookingStepper"
import useRouteParams from "@/hooks/useRouteParams"
import { LocalApi, ParamUseStateType } from "@/types/common"
import { wait } from "@/utils/others"
import $request from "@/utils/request"
import { getScheduleByDate } from "@/utils/requestFunctions"
import { Divider } from "@mui/material"
import { AxiosError } from "axios"
import { useRouter } from "next/router"
import { useEffect, useMemo, useState } from "react"

export default function BookingPage() {
  const router = useRouter()
  const { open } = useFeedbackBar()
  const routeParams = useRouteParams(["step"])
  const { nextStep, previousStep, activeStep } = useBookingStepper()
  const [params, paramsSet] = useState<ParamUseStateType["0"]>({ ...routeParams } as never)
  const [bookingPayload, bookingPayloadSet] = useState<LocalApi.BookingPayload>()

  useEffect(() => {
    console.log("params", params)
    console.log("routeParams", routeParams)
  }, [params, routeParams])

  const onClick = useButtonClickCallback(async () => {
    switch (activeStep) {
      case 0: {
        nextStep(params)
        return
      }
      case 1: {
        nextStep(params)
        return
      }
      case 2: {
        if (bookingPayload) {
          try {
            const resp = await $request<
              "LocalApiDocs",
              WroomCommonTypes.ResponseMessageData,
              LocalApi.BookingPayload
            >("LocalApiDocs", "book", {
              method: "POST",
              data: {
                ...bookingPayload,
              },
            })
            open(`成功掛號！預約號碼：${resp.data.Message}`)
            await wait(3000)
            router.push("/")
          } catch (error) {
            const _error = error as AxiosError
            const errorResponseData = _error.response?.data as WroomCommonTypes.ResponseMessageData
            open(errorResponseData.Message, { alertSeverity: "error", autoHideDuration: 5000 })
          } finally {
            nextStep(params)
          }
        }
        return
      }
    }
  }, [nextStep, params, bookingPayload])

  const CurrentMainContent = useMemo(() => {
    switch (activeStep) {
      case 0: {
        return <BookingPickDate paramsSet={paramsSet} />
      }
      case 1: {
        return <BookingPickSchedule paramsSet={paramsSet} />
      }
      case 2: {
        return <BookingConfirmation bookingPayloadSet={bookingPayloadSet as never} />
      }
    }
  }, [activeStep])

  const CurrentButton = useMemo(() => {
    console.log(params)

    switch (activeStep) {
      case 0: {
        const isOk = params?.date != undefined
        return isOk && <CommonButton onClick={onClick}>選擇門診</CommonButton>
      }
      case 1: {
        const isOk = params?.date != undefined && params?.scheIndex != undefined
        return isOk && <CommonButton onClick={onClick}>掛號確認</CommonButton>
      }
      case 2: {
        return <CommonButton onClick={onClick}>掛號</CommonButton>
      }
    }
  }, [activeStep, onClick, params])

  return (
    <PageBox sx={{ mb: 5 }}>
      {CurrentMainContent}
      <Divider sx={{ m: 3 }} />
      {CurrentButton}
      {activeStep > 0 && <CommonButton onClick={previousStep as never}>返回</CommonButton>}
    </PageBox>
  )
}

BookingPage.auth = true

export async function getServerSideProps(context: globalThis.Next.GetServerSidePropsContext) {
  const step = Number(context.query.step) as number as 0 | 1 | 2
  console.log(context.params)
  console.log(context.query)

  const data = await (async () => {
    switch (step) {
      case 0: {
        return {}
      }
      case 1: {
        const schedule = await getScheduleByDate(context.query.date as string)
        return {
          schedule,
        }
      }
      case 2: {
        const schedule = await getScheduleByDate(context.query.date as string)

        const schedulePicked = schedule.find(
          (s) => Number(s.Index) === Number(context.query.scheIndex)
        )

        return {
          schedulePicked,
          datePicked: context.query.date,
        }
      }
      default: {
        return {}
      }
    }
  })()

  return {
    props: {
      data: {
        ...data,
        step,
      },
    },
  }
}
