import { Fragment, useMemo, useState } from "react"
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import { useSession } from "next-auth/react"
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Fade,
  Grow,
  Slide,
  Zoom,
} from "@mui/material"
import ExpandMoreIcon from "@mui/icons-material/ExpandMoreRounded"

export default function ProfileCard() {
  const session = useSession()
  const [expanded, setExpanded] = useState<boolean>(false)
  const profile = useMemo(() => session.data?.user, [session.data?.user])
  const infoList = useMemo<{ label: string; content?: string }[]>(
    () => [
      {
        label: "身分證字號",
        content: profile?.identity_id,
      },
      {
        label: "生日",
        content: profile?.birth_date,
      },
      {
        label: "註冊信箱（找回密碼用）",
        content: profile?.email,
      },
      {
        label: "手機號碼",
        content: profile?.phone,
      },
    ],
    [profile?.birth_date, profile?.email, profile?.identity_id, profile?.phone]
  )

  return (
    <Accordion expanded={expanded} onChange={(b) => setExpanded((s) => !s)}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>基本資料</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Card sx={{ width: "100%" }}>
          <CardContent>
            <Slide direction="down" in={expanded} timeout={1000}>
              <Typography variant="h5" component="div">
                {session.data?.user.name}
              </Typography>
            </Slide>
            {infoList.map((line, indexLine) => (
              <Fragment key={`profile-card-info-line-${line.label}-${indexLine}`}>
                <Slide direction="left" in={expanded} timeout={1000}>
                  <Box>
                    <Typography variant="caption" component="label">
                      {line.label}
                    </Typography>
                    <Typography component="p" color="text.secondary">
                      {line.content}
                    </Typography>
                  </Box>
                </Slide>
              </Fragment>
            ))}
          </CardContent>
        </Card>
      </AccordionDetails>
    </Accordion>
  )
}
