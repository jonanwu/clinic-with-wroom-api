import usePageTitle from "@/hooks/usePageTitle"
import Head from "next/head"

export default function Helmet() {
  const title = usePageTitle()

  return (
    <Head>
      <title>{title}</title>
    </Head>
  )
}
