import { Backdrop, CircularProgress } from "@mui/material"
import React from "react"
import componentsScss from "./components.module.scss"
import useComponentsContext from "@/hooks/useComponentsContext"

export default function LoadingMask() {
  const { isLoadingMaskOpen } = useComponentsContext()

  return (
    <Backdrop sx={{ color: "#fff", zIndex: 1 }} open={isLoadingMaskOpen}>
      <CircularProgress className={componentsScss.LoadingCircle} size={80} />
    </Backdrop>
  )
}
