import { TextField } from "@mui/material"
import { MobileDatePicker } from "@mui/x-date-pickers"
import dayjs from "dayjs"
import { useCallback, useEffect, useMemo, useState } from "react"
import CssModule from "./components.module.scss"

type TextFieldProps = Parameters<typeof TextField>["0"]

export type FormTextFieldProps = TextFieldProps & {
  regex?: RegExp
  customType?: "datepicker" | "selection"
  validState?: {
    validSet: ReturnType<typeof useState>["1"]
  }
}

export default function FormTextField(props: FormTextFieldProps) {
  const {
    margin = "normal",
    fullWidth = true,
    validState,
    type = "text",
    customType,
    required,
    regex,
    ...restProps
  } = props

  const initialValue = useMemo(() => {
    switch (customType) {
      case "datepicker":
        return dayjs("1990-01-01")
      default:
        return ""
    }
  }, [customType])

  const [value, valueSet] = useState(initialValue)

  const isValid = useMemo(() => {
    switch (customType) {
      case "datepicker": {
        return !!value.toString()
      }
      default: {
        if (required) {
          if (regex && typeof value === "string") {
            const matched = value.match(regex)
            return !!matched
          }
          return !!value
        }
      }
    }
  }, [customType, regex, required, value])

  const onChange = useCallback<Required<TextFieldProps>["onChange"]>(
    (ev) => {
      switch (customType) {
        case "datepicker":
          valueSet(ev as never)
          break
        default:
          ev.preventDefault()
          valueSet(ev.target.value)
      }
    },
    [customType]
  )

  useEffect(() => {
    if (validState) {
      validState.validSet(isValid)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isValid])

  switch (customType) {
    case "datepicker": {
      return (
        <>
          <MobileDatePicker
            openTo="year"
            label="生日"
            views={["year", "month", "day"]}
            inputFormat="YYYY 年 MM 月 DD 日"
            onChange={onChange as never}
            value={value}
            renderInput={(params) => (
              <TextField
                {...params}
                {...restProps}
                error={!isValid}
                margin={margin}
                fullWidth={fullWidth}
                className={CssModule.MobileDatePicker}
              />
            )}
          />
        </>
      )
    }
    default: {
      return (
        <TextField
          error={!isValid}
          margin={margin}
          value={value}
          fullWidth={fullWidth}
          onChange={onChange}
          type={type}
          {...restProps}
        />
      )
    }
  }
}
