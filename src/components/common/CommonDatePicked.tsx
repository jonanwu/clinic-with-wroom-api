import { Typography } from "@mui/material"

export default function CommonDatePicked(props: { value: string }) {
  return (
    <Typography
      border={1}
      borderRadius={1}
      color="CaptionText"
      sx={{ textAlign: "center", fontSize: 24, m: 0, p: 0 }}
      variant="overline"
      display="block"
      gutterBottom
    >
      {`已選日期: ${props.value}`}
    </Typography>
  )
}
