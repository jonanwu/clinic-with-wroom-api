import { BoxProps, Box } from "@mui/material"

type Props = BoxProps

export default function PageBox(props: Props) {
  const { children, sx, ...rest } = props

  return (
    <Box sx={{ mt: 5, width: "100%", ...sx }} {...rest}>
      {children}
    </Box>
  )
}
