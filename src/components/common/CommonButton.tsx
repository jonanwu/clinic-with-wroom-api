import { Button, ButtonProps } from "@mui/material"

type Props = ButtonProps

export default function CommonButton(props: Props) {
  return (
    <Button fullWidth type="button" variant="contained" sx={{ mt: 1, mb: 1 }} {...props}>
      {props.children}
    </Button>
  )
}
