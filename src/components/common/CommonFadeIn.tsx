import { Box, Grow, GrowProps } from "@mui/material"

type Props = GrowProps

export default function CommonFadeIn(props: Props) {
  const { children, ...rest } = props
  return (
    <Grow timeout={1000} {...rest}>
      <Box>{children}</Box>
    </Grow>
  )
}
