import Others from "@/constants/Others"
import useBookingStepper from "@/hooks/useBookingStepper"
import { Step, StepLabel, Stepper } from "@mui/material"
import { useRouter } from "next/router"
import { useCallback } from "react"

export const steps = Object.values(Others.bookingStep)

export default function BookingStepper() {
  const { activeStep } = useBookingStepper()
  const router = useRouter()
  const onStepLabelClick = useCallback(
    (step: number) => {
      if (activeStep > step)
        router.push({ pathname: router.pathname, query: { ...router.query, step } })
    },
    [activeStep, router]
  )

  return (
    <Stepper sx={{ width: "100%", textAlign: "center" }} activeStep={activeStep}>
      {steps.map((label, labelIndex) => (
        <Step
          sx={{
            "& .MuiStepLabel-root .Mui-completed": {
              color: "secondary.dark", // circle color (COMPLETED)
            },
            "& .MuiStepLabel-label.Mui-completed.MuiStepLabel-alternativeLabel": {
              color: "grey.500", // Just text label (COMPLETED)
            },
            "& .MuiStepLabel-root .Mui-active": {
              color: "secondary.main", // circle color (ACTIVE)
            },
            "& .MuiStepLabel-label.Mui-active.MuiStepLabel-alternativeLabel": {
              color: "common.white", // Just text label (ACTIVE)
            },
            "& .MuiStepLabel-root .Mui-active .MuiStepIcon-text": {
              fill: "black", // circle's number (ACTIVE)
            },
          }}
          key={label}
        >
          <StepLabel onClick={() => onStepLabelClick(labelIndex)}>{label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  )
}
