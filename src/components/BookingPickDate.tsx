import useRouteParams from "@/hooks/useRouteParams"
import { ParamUseStateType } from "@/types/common"
import { Divider, TextField } from "@mui/material"
import { StaticDatePicker } from "@mui/x-date-pickers"
import dayjs, { Dayjs } from "dayjs"
import { Fragment, useEffect, useMemo, useState } from "react"
import CommonDatePicked from "./common/CommonDatePicked"

type Props = {
  paramsSet: ParamUseStateType["1"]
}

export default function BookingPickDate(props: Props) {
  const routeParams = useRouteParams(["step"])
  const [value, setValue] = useState<Dayjs | null>(
    dayjs((routeParams.date as string) || Date.now())
  )

  const dateString = useMemo(() => value?.format("YYYY-MM-DD"), [value])

  useEffect(() => {
    props.paramsSet({ date: dateString })
  }, [dateString, props, value])

  return (
    <Fragment>
      <CommonDatePicked value={dateString || ""} />
      <Divider sx={{ m: 3 }} />
      <StaticDatePicker
        displayStaticWrapperAs="desktop"
        inputFormat="YYYY-MM-DD"
        openTo="day"
        views={["year", "month", "day"]}
        value={value}
        onChange={(newValue) => {
          setValue(newValue)
        }}
        renderInput={(params) => <TextField {...params} />}
        shouldDisableDate={(day) => {
          const conditions = [day.isBefore(dayjs().startOf("day")), day.get("day") === 0]
          return conditions.some((t) => t)
        }}
        shouldDisableMonth={(day) => day.isBefore(dayjs().startOf("month"))}
        shouldDisableYear={(day) => day.isBefore(dayjs().startOf("year"))}
      />
    </Fragment>
  )
}
