import { useFeedbackBar } from "@/hooks/otherHooks"
import { Alert, Snackbar } from "@mui/material"
import { useCallback } from "react"

export default function FeedbackBar() {
  const { props, close } = useFeedbackBar()

  const handleClose = useCallback(() => close(), [close])

  const { message, alertSeverity, ...restProps } = props

  return (
    <Snackbar open={props.open} autoHideDuration={2000} onClose={handleClose} {...restProps}>
      <Alert onClose={handleClose} severity={alertSeverity} sx={{ width: "100%" }}>
        {message}
      </Alert>
    </Snackbar>
  )
}
