import { WroomRegular } from "@/constants/Others"
import { ButtonProps, Card, CardActions, CardContent, Typography } from "@mui/material"
import CommonButton from "./common/CommonButton"

export default function ScheduleCard(props: {
  sche: WroomCommonTypes.Sche
  onPickSche?: Required<ButtonProps>["onClick"]
  onCancelSche?: Required<ButtonProps>["onClick"]
}) {
  const { sche: s, onPickSche, onCancelSche } = props
  return (
    <Card sx={{ p: 1 }} variant="outlined">
      <CardContent>
        <Typography variant="h4" sx={{ mb: 2 }} color="text.primary">
          {s.DoctorName}
        </Typography>
        <Typography variant="h6" sx={{ mb: 0 }} color="text.secondary">
          {s.SubjectName}
        </Typography>
        <Typography variant="h6" sx={{ mb: 1 }} color="text.secondary">
          {`${WroomRegular.TsecMap[s.Tsec]}`}
        </Typography>
        <Typography
          variant="h4"
          sx={{ mb: 0, textAlign: "right", width: "100%" }}
          color="text.primary"
        >
          {`${s.MedRoomName}`}
        </Typography>
        <Typography
          variant="caption"
          sx={{ mb: 0, textAlign: "right", width: "100%" }}
          color="text.secondary"
        >
          {`本日已約診人數：${s.Count}`}
        </Typography>
        <CardActions>
          {onPickSche && (
            <CommonButton onClick={onPickSche} sx={{ mb: 0, mt: 2 }} size="small">
              預約
            </CommonButton>
          )}
          {onCancelSche && (
            <CommonButton onClick={onCancelSche} sx={{ mb: 0, mt: 2 }} size="small">
              重新選擇
            </CommonButton>
          )}
        </CardActions>
      </CardContent>
    </Card>
  )
}
