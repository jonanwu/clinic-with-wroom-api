import usePageProps from "@/hooks/usePageProps"
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
  Box,
  Slide,
} from "@mui/material"
import { Fragment, useCallback, useEffect, useMemo, useState } from "react"
import ExpandMoreIcon from "@mui/icons-material/ExpandMore"
import { WroomRegular } from "@/constants/Others"
import ScheduleCard from "@/components/ScheduleCard"
import CommonFadeIn from "./common/CommonFadeIn"
import { ParamUseStateType } from "@/types/common"
import { useSession } from "next-auth/react"
import useRouteParams from "@/hooks/useRouteParams"

type Props = {
  paramsSet: ParamUseStateType["1"]
}

export default function BookingPickSchedule(props: Props) {
  const routeParams = useRouteParams(["step"])
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { date, ...restParams } = routeParams
  const session = useSession()
  const { data } = usePageProps<{ schedule: WroomCommonTypes.Sche[] }>()
  const schedule = useMemo(() => data?.schedule || [], [data])
  const [expanded, setExpanded] = useState<number | boolean>(false)
  const selectedSche = useMemo(
    () => data?.schedule.find((s) => Number(s.Index) === Number(restParams.scheIndex)),
    [data?.schedule, restParams.scheIndex]
  )
  const [schePicked, schePickedSet] = useState<WroomCommonTypes.Sche | null>(selectedSche as never)

  useEffect(() => {
    if (schePicked && session.data) {
      props.paramsSet((pre) => ({
        ...pre,
        scheIndex: schePicked.Index,
      }))
    }
  }, [props, schePicked, session.data])

  const handleChange = useCallback(
    (index: number) => () => {
      setExpanded((s) => {
        if (index === s) {
          return false
        }
        return index
      })
    },
    []
  )

  const generateSummaryText = useCallback(
    (index: number, s: WroomCommonTypes.Sche) => {
      if (expanded !== index) {
        return ` ${WroomRegular.TsecMap[s.Tsec]} - ${s.SubjectName} - ${s.DoctorName}`
      }
      return "門診詳細資訊"
    },
    [expanded]
  )

  const onPickSche = useCallback(
    (sche: WroomCommonTypes.Sche) => () => {
      schePickedSet(sche)
    },
    []
  )
  const onCancelSche = useCallback(() => {
    schePickedSet(null)
  }, [])

  const isScheduled = useMemo(() => !!schePicked, [schePicked])

  const [inIndex, inIndexSet] = useState(0)

  return (
    <Box sx={{ mb: 5 }}>
      {!isScheduled && (
        <Box>
          {schedule.length ? (
            schedule.map((s, sIndex) => (
              <Fragment key={`BookingPickSchedule-${sIndex}-${s.DoctorId}`}>
                <Slide
                  direction="left"
                  in={!isScheduled && inIndex >= sIndex}
                  onEnter={() => inIndexSet(sIndex + 1)}
                  timeout={1000 + 100 * sIndex}
                >
                  <Accordion expanded={expanded === sIndex} onChange={handleChange(sIndex)}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                      <Typography>{generateSummaryText(sIndex, s)}</Typography>
                    </AccordionSummary>
                    <AccordionDetails sx={{ p: 3, pt: 0 }}>
                      <ScheduleCard onPickSche={onPickSche(s)} sche={s} />
                    </AccordionDetails>
                  </Accordion>
                </Slide>
              </Fragment>
            ))
          ) : (
            <Typography>當天無門診哦！</Typography>
          )}
        </Box>
      )}
      {isScheduled && (
        <CommonFadeIn in={isScheduled}>
          <ScheduleCard onCancelSche={onCancelSche} sche={schePicked as never} />
        </CommonFadeIn>
      )}
    </Box>
  )
}
