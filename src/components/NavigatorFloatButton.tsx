import { useCallback } from "react"
import SpeedDial from "@mui/material/SpeedDial"
import SpeedDialIcon from "@mui/material/SpeedDialIcon"
import SpeedDialAction from "@mui/material/SpeedDialAction"
import IconHome from "@mui/icons-material/Home"
import IconArrowBack from "@mui/icons-material/ArrowBack"
import IconArrowForward from "@mui/icons-material/ArrowForward"
import LogoutOutlined from "@mui/icons-material/LogoutOutlined"
import { useRouter } from "next/router"
import { signOut } from "next-auth/react"

const iconName = {
  "Go-Home": "Go-Home",
  "Go-Back": "Go-Back",
  "Go-Forward": "Go-Forward",
  "Log-Out": "Log-Out",
}

const iconTitle = {
  "Go-Home": "回首頁",
  "Go-Back": "返回",
  "Go-Forward": "前一頁",
  "Log-Out": "登出",
}

const actions = [
  { icon: <IconHome />, name: iconName["Go-Home"] },
  { icon: <IconArrowBack />, name: iconName["Go-Back"] },
  { icon: <IconArrowForward />, name: iconName["Go-Forward"] },
  { icon: <LogoutOutlined />, name: iconName["Log-Out"] },
] as { icon: JSX.Element; name: keyof typeof iconName }[]

export default function NavigatorFloatButton() {
  const router = useRouter()
  const onClick = useCallback(
    (name: keyof typeof iconName) => () => {
      switch (name) {
        case "Go-Home": {
          router.push("/")
          break
        }
        case "Go-Back": {
          router.back()
          break
        }
        case "Go-Forward": {
          history.forward()
          break
        }
        case "Log-Out": {
          signOut()
          break
        }
      }
    },
    [router]
  )

  return (
    <SpeedDial
      ariaLabel="SpeedDial basic example"
      sx={{ position: "fixed", bottom: 20, right: 20 }}
      icon={<SpeedDialIcon />}
    >
      {actions.map((action) => (
        <SpeedDialAction
          onClick={onClick(action.name)}
          key={action.name}
          icon={action.icon}
          tooltipTitle={iconTitle[action.name]}
        />
      ))}
    </SpeedDial>
  )
}
