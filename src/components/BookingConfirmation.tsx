import usePageProps from "@/hooks/usePageProps"
import { Box, Divider } from "@mui/material"
import { useSession } from "next-auth/react"
import ScheduleCard from "./ScheduleCard"
import CommonDatePicked from "./common/CommonDatePicked"
import { useEffect, useMemo } from "react"
import { BookingPayloadSet } from "@/types/common"

type Props = {
  bookingPayloadSet: BookingPayloadSet["1"]
}

export default function BookingConfirmation(props: Props) {
  const session = useSession()
  const pageProps = usePageProps<{ schedulePicked: WroomCommonTypes.Sche; datePicked: string }>()

  const propsData = useMemo(() => pageProps.data, [pageProps.data])

  useEffect(() => {
    if (propsData?.datePicked && propsData.schedulePicked && session.data?.user) {
      props.bookingPayloadSet({
        ReservationDate: propsData.datePicked,
        Tsec: propsData.schedulePicked.Tsec,
        DoctorId: propsData.schedulePicked.DoctorId,
        CId: session.data.user.identity_id,
        CName: session.data.user.name,
        Phone: session.data.user.phone,
        Birthday: session.data.user.birth_date,
      })
    }
  }, [
    //
    props,
    propsData?.datePicked,
    propsData?.schedulePicked,
    session.data?.user,
  ])

  return (
    <Box sx={{ mb: 5, p: 2 }}>
      <CommonDatePicked value={pageProps.data?.datePicked || ""} />
      <Divider sx={{ m: 5 }} />
      <ScheduleCard sche={propsData?.schedulePicked as never} />
    </Box>
  )
}
