import { CustomAppProps } from "@/types/common"
import { createContext, useContext } from "react"

type PagePropsContextType<T> = Omit<CustomAppProps<T>["pageProps"], "session">

export const initialValue: PagePropsContextType<unknown> = { data: undefined }

const PagePropsContext = createContext<PagePropsContextType<unknown>>(initialValue)

export function PagePropsContextProvider(props: Parameters<typeof PagePropsContext.Provider>["0"]) {
  return <PagePropsContext.Provider value={props.value}>{props.children}</PagePropsContext.Provider>
}

export default function usePageProps<T = unknown>() {
  const context = useContext<PagePropsContextType<T>>(PagePropsContext as never)
  return context
}
