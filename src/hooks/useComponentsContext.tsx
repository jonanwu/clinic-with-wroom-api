import {
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from "react"

type ComponentsContextStateType = {
  isLoadingMaskOpen: boolean
  isFeedbackBarOpen: boolean
}

type ComponentsContextType = ComponentsContextStateType & {
  update: Dispatch<SetStateAction<ComponentsContextStateType>>
}

const initialContext: ComponentsContextStateType = {
  isLoadingMaskOpen: false,
  isFeedbackBarOpen: false,
}

const ComponentsContext = createContext<ComponentsContextType>({
  ...initialContext,
  update: (s) => ({ ...s }),
})

export function ComponentsStateProvider(props: PropsWithChildren) {
  const [state, update] = useState(initialContext)

  return (
    <ComponentsContext.Provider
      value={{
        ...state,
        update,
      }}
    >
      {props.children}
    </ComponentsContext.Provider>
  )
}

export default function useComponentsContext() {
  const context = useContext(ComponentsContext)

  return context
}
