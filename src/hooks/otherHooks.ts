import { useCallback } from "react"
import { ButtonProps, SnackbarProps } from "@mui/material"
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux"
import type { RootState } from "@/store/store"
import { ComponentAction, ComponentsStateType } from "@/store/types"
import { Dispatch } from "@reduxjs/toolkit"
import { useRouter } from "next/router"

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export function useAppDispatch<T extends ComponentAction>() {
  return useDispatch<Dispatch<T>>()
}
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export function useOthersManagement() {
  const dispatch = useAppDispatch()
  const othersState = useAppSelector((state) => state.components.others)

  const update = useCallback(
    (state: Partial<ComponentsStateType["others"]>) => {
      dispatch({
        type: "UPDATE",
        target: "others",
        payload: {
          ...othersState,
          ...state,
        },
      })
    },
    [dispatch, othersState]
  )

  return { update, state: othersState }
}

export function useFeedbackBar() {
  const dispatch = useAppDispatch()
  const props = useAppSelector((state) => state.components.feedbackProps)
  const propsSet = useCallback(
    (_props: SnackbarProps) => {
      dispatch({
        type: "UPDATE",
        target: "feedbackProps",
        payload: {
          ...props,
          ..._props,
        },
      })
    },
    [dispatch, props]
  )

  const open = useCallback(
    (message?: string, options?: Omit<ComponentsStateType["feedbackProps"], "message">) => {
      dispatch({
        type: "UPDATE",
        target: "feedbackProps",
        payload: {
          message,
          open: true,
          ...options,
        },
      })
    },
    [dispatch]
  )
  const close = useCallback(() => {
    dispatch({
      type: "UPDATE",
      target: "feedbackProps",
      payload: {
        ...props,
        open: false,
      },
    })
  }, [dispatch, props])

  const message = useCallback(
    (message: string) => {
      dispatch({
        type: "UPDATE",
        target: "feedbackProps",
        payload: {
          message,
        },
      })
    },
    [dispatch]
  )

  return {
    open,
    close,
    message,
    props,
    propsSet,
  }
}

export function useButtonClickCallback(
  ...args: Parameters<typeof useCallback<Required<ButtonProps>["onClick"]>>
) {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useCallback(...args)
}

export function useFirstPathname() {
  const router = useRouter()
  return router.pathname.split(/\//g)[1]
}
