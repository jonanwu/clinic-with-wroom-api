import { useCallback, useMemo } from "react"
import { useRouter } from "next/router"
import Others from "@/constants/Others"
import { ParamUseStateType } from "@/types/common"
import queryString from "query-string"
import { useAppDispatch } from "./otherHooks"

const steps = Object.keys(Others.bookingStep)

export default function useBookingStepper() {
  const dispatch = useAppDispatch()
  const router = useRouter()
  const activeStep = useMemo(() => Number(router.query.step || 0), [router])
  const nextStep = useCallback(
    (params: ParamUseStateType["0"]) => {
      const __nextStep = Number(router.query.step) + 1
      if (steps[__nextStep]) {
        dispatch({
          type: "UPDATE",
          target: "test",
          payload: {
            text: `${__nextStep}`,
          },
        })
        router.push({
          pathname: `/booking/${__nextStep}`,
          search: queryString.stringify(params as never),
        })
      }
    },
    [dispatch, router]
  )
  const previousStep = useCallback(() => {
    router.back()
  }, [router])
  return { nextStep, previousStep, activeStep }
}
