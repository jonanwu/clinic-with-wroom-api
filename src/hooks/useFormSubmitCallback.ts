import { DependencyList, FormEvent, useCallback } from "react"
import useComponentsContext from "./useComponentsContext"
import { useFeedbackBar } from "./otherHooks"

type ContinueObjectType = {
  isSuccess: boolean
  message?: string
  callback?: () => void
}

type CallbackType = (
  // eslint-disable-next-line no-unused-vars
  event: FormEvent<HTMLFormElement>
) => Promise<ContinueObjectType>

export default function useFormSubmitCallback(callback: CallbackType, deps: DependencyList = []) {
  const { update } = useComponentsContext()
  const { open } = useFeedbackBar()
  const handleSubmit = useCallback(
    async (event: FormEvent<HTMLFormElement>) => {
      update((s) => ({ ...s, isLoadingMaskOpen: true }))
      event.preventDefault()
      const continueObject = await callback(event)
      open(continueObject.message, {
        alertSeverity: continueObject.isSuccess ? "success" : "error",
      })
      update((s) => ({ ...s, isLoadingMaskOpen: false }))
      if (continueObject.callback) continueObject.callback()
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [callback, open, ...deps]
  )

  return handleSubmit
}
