import { useRouter } from "next/router"
import { useMemo } from "react"

export default function useRouteParams(toRemove: string[]) {
  const router = useRouter()
  const output = useMemo(() => {
    const params = { ...router.query }
    for (let i = 0; i < toRemove.length; i++) {
      delete params[toRemove[i]]
    }
    return params
  }, [router.query, toRemove])
  return output
}
