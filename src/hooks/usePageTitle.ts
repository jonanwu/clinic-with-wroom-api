import Others from "@/constants/Others"
import { useMemo } from "react"
import { useFirstPathname } from "./otherHooks"
import { steps as bookingSteps } from "@/components/BookingStepper"
import useBookingStepper from "./useBookingStepper"

export default function usePageTitle() {
  const { activeStep } = useBookingStepper()
  const pathname = useFirstPathname()
  const title = useMemo(() => {
    switch (pathname) {
      case "booking": {
        const output = `${Others.headTitle[pathname as never]}(${bookingSteps[activeStep]})`
        return output
      }
      default: {
        return Others.headTitle[pathname as never]
      }
    }
  }, [activeStep, pathname])

  return title
}
