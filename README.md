
# 診所預約系統

整合 [Wroom API](https://cloudregapi.vision.com.tw/Help)

## Techs

- [Pnpm](https://pnpm.io/)
- [NextJS](https://nextjs.org/)
- [PrismaJS](https://www.prisma.io/)
- [SQLite](https://sqlite.org/index.html)
- [Material UI](https://mui.com/)

## Installation

```bash
pnpm i
```

## Getting Started

First, run the development server:

```bash
pnpm run dev
```

